--------------------------------------------------------------------------------------------------------------------------------------------------------------
This is a kinect controlled game developed on Unity 3D 4.1(pro version as if you are editing then some of its features are not available in free version).
--------------------------------------------------------------------------------------------------------------------------------------------------------------
Some important notes:
1.Only scenes 'gui_main','About' and 'terrain_effects' are part of the original game with the scene 'gui_main' with index 0.Others scenes were just for 
 testing and development purpose.
2.This repository contains only the final game and not the developement process.For history and development of this game please go through its documentation at
 http://students.iitk.ac.in/projects/programmingclub:2013:emotiv
 and the codes at https://github.com/krgaurav94/Kinect-Game
--------------------------------------------------------------------------------------------------------------------------------------------------------------

GamePlay
---------------------------------------------------------------------------------------------------------------------------------------------------------------
The kinect is used to avoid obstacles and falling.While the keys i,u,r,t,y,g are for obstacles generation.